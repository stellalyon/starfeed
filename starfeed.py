import configparser
import webbrowser
from pathlib import Path

import feedparser
from gi.repository import GLib, Gtk


class Feed(Gtk.TreeView):
    def __init__(self, url):
        super().__init__()
        self.set_headers_visible(False)

        store = Gtk.ListStore(str, str)
        self.set_model(store)
        pane_cell = Gtk.CellRendererText()
        pane_column = Gtk.TreeViewColumn('Articles', pane_cell, text=0)
        self.append_column(pane_column)

        self.connect('row-activated', self.activated)

        for entry in feedparser.parse(url).entries:
            store.append((entry.title, entry.link))

    def activated(self, treeview, path, view):
        webbrowser.open(treeview.props.model[path][1])


class Window(Gtk.Window):
    def __init__(self, application):
        super().__init__(application=application)
        self.set_title('Starfeed')

        self.config = configparser.ConfigParser()
        self.config.read(Path.home() / '.config' / 'plop')

        hbox = Gtk.HBox()
        feed_list = Gtk.StackSidebar()
        self.stack = Gtk.Stack()
        feed_list.set_stack(self.stack)

        hbox.pack_start(feed_list, False, False, 0)
        hbox.pack_start(self.stack, True, True, 0)
        self.add(hbox)

    def update(self):
        active = self.stack.get_visible_child_name()
        for child in self.stack.get_children():
            self.stack.remove(child)
        for section in self.config.sections():
            scroll = Gtk.ScrolledWindow()
            feed = Feed(self.config[section]['url'])
            scroll.add(feed)
            self.stack.add_titled(scroll, section, section)
        self.show_all()
        if active:
            self.stack.set_visible_child_name(active)


class Starfeed(Gtk.Application):
    def do_activate(self):
        self.window = Window(self)
        self.window.connect('destroy', lambda window: self.quit())
        self.window.update()
        GLib.timeout_add_seconds(300, lambda: self.window.update() or True)


Starfeed().run()
